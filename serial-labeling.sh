#!/bin/bash
######################################################
# Script for getting serial number for components related to Humidity System for the ITk
# Written by loan.truong@cern.ch on June 4th 2024
#
# 5 different functions for 5 different serial numbers
######################################################

1_itkCompartment(){
    while true; do
	ask="$(zenity --list --column "" --title="ITk compartment Options" \
        "Strip-barrel" \
        "Strip-Endcap" \
        "Outer Pixel" \
        "Inner Pixel" \
        "OSV" \
        --width=100 --height=300)"

	case $ask in
            "Strip-barrel")
		echo "You select $ask"
		n1=1
		break
		;;
            "Strip-Endcap")
		echo "You select $ask"
		n1=2
		break
		;;
            "Outer Pixel")
		echo "You select $ask"
		n1=3
		break
		;;
            "Inner Pixel")
		echo "You select $ask"
		n1=4
		break
		;;
            "OSV")
		echo "You select $ask"
		n1=5
		break
		;;
            *)
		echo "Wrong selection"
		;;
	esac
    done
    echo "Your 1st serial is: $n1"
    return $n1
}

2_detectorSide(){
    while true; do
	ask="$(zenity --list --column "" --title="Which detector Side" \
    	    "A" \
    	    "C" \
	    --width=100 --height=300)"

	case $ask in
            "A")
		echo "You select $ask"
		n2=1
		break
		;;
            "C")
		echo "You select $ask"
		n2=2
		break
		;;
            *)
		echo "Wrong selection"
		;;
	esac
    done
    echo "Your 2st serial is: $n2"
    return $n2
}

3_fixationPosition(){
    while true; do
	ask="$(zenity --list --column "" --title="Fixation Position" \
        "Mapped point 1" \
        "Mapped point 2" \
	"Mapped point 3" \
	"Mapped point 4" \
	"Mapped point 5" \

        --width=100 --height=300)"

	case $ask in
            "Mapped point 1")
		echo "You select $ask"
		n3=1
		break
		;;
            "Mapped point 2")
		echo "You select $ask"
		n3=2
		break
		;;
            "Mapped point 3")
		echo "You select $ask"
		n3=3
		break
		;;
            "Mapped point 4")
		echo "You select $ask"
		n3=4
		break
            ;;
            "Mapped point 5")
		echo "You select $ask"
		n3=5
		break
		;;
            *)
		echo "Wrong selection"
		;;
	esac
    done

    echo "Your 3rd serial is: $n3"
    return $n3
}

4_PinsInMTPorDB25(){
    while true; do
	ask="$(zenity --list --column "" --title="Pins in MTP or DB25" \
        "Whole MTP, no pin" \
        "Pin MTP 1" \
	"Pin MTP 2" \
	"Pin MTP 3" \
	"Pin MTP 4" \
	"Pin MTP 5" \
	"Pin MTP 6" \
	"Pin MTP 7" \
	"Pin MTP 8" \
	"Pin MTP 9" \
	"Pin MTP 10" \
	"Pin MTP 11" \
	"Pin MTP 12" \
        "FOS FBG side 1" \
	"FOS FBG side 2" \
	"PT10k pin 0" \
	"PT10k pin 1" \
	"HiH4000 ground (black)" \
	"HiH4000 voltage supply (red)" \
	"HiH4000 signal (green)" \
        --width=100 --height=700)"

	case $ask in
            "Whole MTP, no pin")
                echo "You select $ask"
                n4=0
                break
                ;;
	    "Pin MTP 1")
		echo "You select $ask"
		n4=1
		break
		;;
	    "Pin MTP 2")
		echo "You select $ask"
		n4=2
		break
		;;
	    "Pin MTP 3")
		echo "You select $ask"
		n4=3
		break
		;;
	    "Pin MTP 4")
		echo "You select $ask"
		n4=4
		break
		;;
	    "Pin MTP 5")
		echo "You select $ask"
		n4=5
		break
		;;
	    "Pin MTP 6")
		echo "You select $ask"
		n4=6
		break
		;;
	    "Pin MTP 7")
		echo "You select $ask"
		n4=7
		break
		;;
	    "Pin MTP 8")
		echo "You select $ask"
		n4=8
		break
		;;
	    "Pin MTP 9")
		echo "You select $ask"
		n4=9
		break
		;;
	    "Pin MTP 10")
		echo "You select $ask"
		n4=10
		break
		;;
	    "Pin MTP 11")
		echo "You select $ask"
		n4=11
		break
		;;
	    "Pin MTP 12")
		echo "You select $ask"
		n4=12
		break
		;;
	    "FOS FBG side 1")
		echo "You select $ask"
		n4=13
		break
		;;
	    "FOS FBG side 2")
		echo "You select $ask"
		n4=14
		break
		;;
	    "PT10k pin 0")
		echo "You select $ask"
		n4=15
		break
		;;
            "PT10k pin 1")
		echo "You select $ask"
		n4=16
		break
		;;
	    "HiH4000 ground (black)")
		echo "You select $ask"
		n4=17
		break
		;;
            "HiH4000 voltage supply (red)")
		echo "You select $ask"
		n4=18
		break
		;;
	    "HiH4000 signal (green)")
		echo "You select $ask"
		n4=19
		break
		;;
            *)
		echo "Wrong selection"
		;;
	esac
    done
    echo "Your 4th serial is: $n4"
    return $n4
}

5_incrementSupply(){
    while true; do
	inputStr=$(zenity --entry --title="Enter your supply number (incrementing of your production including breaking one)" \
			  --text="Number, eg. 1 if it's your 1st package, 10 if it's your 10th package (even if 8th is broken):" \
                          --width=1000 --height=100)
	if [[ "$inputStr" =~ ^[0-9]+$ ]]
	then
	    n5=$inputStr
	    break
	fi
    done
    echo "Your 5th serial is: $n5"
    return $n5
}

##########
# MAIN
##########
1_itkCompartment
s1=$?

2_detectorSide
s2=$?

3_fixationPosition
s3=$?

4_PinsInMTPorDB25
s4=$?

5_incrementSupply
s5=$?

echo -e "\033[36m Your serial number is $s1-$s2-$s3-$s4-$s5"
echo -e "\033[00m"
