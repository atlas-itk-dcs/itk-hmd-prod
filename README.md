# itk-humidity-production

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Description:
- This is the common area for different scripts used for production process

## ATTENTION:
- The mapping points are not ready yet.
- For now reserve Mapping point 1 and Mapping point 2 for the 2 MTP-12 in Strip EndCaps
- Will have to cross check with Strip EndCap-Barrel and Pixels to build better mappings.

## Status:
- serial-labeling.sh: (STILL WIP)
    - this script is for getting a serial number for a package/bracket, still WIP, have to finalise the labeling scheme first
    - to run: `./serial-labeling.sh`
    - it runs on GNOME (like most of Linux system)
        - If you have a MAC, try to install `zenity` before running it
